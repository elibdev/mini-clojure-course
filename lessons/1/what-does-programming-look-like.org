* What Does Programming Look Like?

While everything is downloading, let's take a little detour into the past and the possible future where the act of programming looks different than it looks today. 

[[http://i.ytimg.com/vi/uxKmDWDUZ5A/maxresdefault.jpg]]

/[[https://www.youtube.com/watch?v=uxKmDWDUZ5A][Hacking and Drinking in The Social Network]]/

There are many ways you can program. The first (general-purpose electronic) computer was made in World War II, and was called the ENIAC. To run a program on the ENIAC, you had to flip switches and connect cables, just like a modular synthesizer. 

[[http://www.columbia.edu/cu/computinghistory/eniac1.jpg]]

/[[http://www.columbia.edu/cu/computinghistory/eniac.html][Programming the ENIAC]]/

Soon after that, we started feeding our programs into our computers using stacks of punchcards. The [[https://en.wikipedia.org/wiki/Jacquard_loom#Importance_in_computing][very same punchcards]] that were invented for feeding patterns into Jacquard looms. 

[[http://www-03.ibm.com/ibm/history/ibm100/images/icp/Q969732S50503Q79/us__en_us__ibm100__punched_card__hand_cards__620x350.jpg]]

/[[http://www-03.ibm.com/ibm/history/ibm100/us/en/icons/punchcard/breakthroughs/][Punchcard stack]]/

Then we moved to storing our programs digitally on hard drives, writing our programs to discs like this:

[[http://www.storagenewsletter.com/wp-content/uploads/old/0icono12/ibmramac_der_01.jpg.pagespeed.ce.PrvSfi9rM9.jpg]]

/[[http://www.storagenewsletter.com/rubriques/hard-disk-drives/document-first-hdd-form-factor-introduction/][Storage disc from the IBM 305 RAMAC released in 1956]]/

Our discs have gotten smaller, and they usually live locked away in metal boxes in our computers, but it's still the same idea. 

[[http://4.bp.blogspot.com/_R7YKf6kY6DY/TNgnm2dUhxI/AAAAAAAAADA/79E9PMpWKxo/s400/800px-Apertura_hard_disk_04.jpg]]

/[[http://www.scrapmetaljunkie.com/269/how-to-scrap-hard-drives-2][Uncovered hard drive]]/

Maybe in the future programming will look exactly like it does now, only in *[[https://www.youtube.com/watch?v=SKPYx4CEIlM][VIRTUAL REALITY]]!*
