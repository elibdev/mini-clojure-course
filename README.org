
* Programming Visuals in Clojure

Welcome! I hope you're all excited to get started making some pretty pictures and thinking about esoteric programming concepts! We'll be assuming no prior programming knowledge, so don't worry if you've never done any programming or had any exposure to it. We'll cover all the concepts you need to do all the exercises. 

It's very important that you ask questions about anything that you're not clear about. I'll send a couple quick questions out for you to answer at the end of each set of exercises so I can get a handle on the pace we should go at and where your interests all are. 

Everybody can go at their own pace. We'll try to keep the necessary work to keep up to a minimum. If you feel like putting in a little bit more sometimes, your understanding and familiarity with the concepts will surely benefit. 

I'll look at and give feedback on all your work, and I'm always happy to help if there's anything you're stuck on. 

* Where to Go When You're Stuck

We have a [[https://clojurevisuals.slack.com/][chat room on Slack]] where we can share ideas, code, and ask questions. 

* Course Goals

1. Have fun and make some sick visuals
2. Learn fundamental programming concepts
3. Develop proficiency in Clojure

* How it'll work

All of the exercises should be able to be completed using the concepts covered up to that point. 

We'll try one set of exercises a week, accompanied by some readings. 

* Exercises

Exercises will generally use the following format:

1. Complete walkthrough (problem broken down, and solved)
2. Lightly guided tutorial (problem broken down, but not solved)
3. Open-ended prompt (problem not broken down or solved)

Some typical exercises might be:

- learn about and explore a new programming concept
- apply a programming concept to create or modify a visual
- debug existing program to make it work
- edit existing program to change or expand functionality
- develop an idea for a visual, then implement it


*If you have any ideas, suggestions, or requests for things to cover or about the course in general, don't keep it to yourself. Please let me know!*
